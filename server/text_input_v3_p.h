/*
    SPDX-FileCopyrightText: 2020 Bhushan Shah <bshah@kde.org>

    SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL
*/
#pragma once

#include "text_input_v3.h"
//#include "clientconnection.h"

#include <QPointer>
#include <QRect>
#include <QVector>
#include <QHash>

#include <wayland-text-input-unstable-v3-server-protocol.h>
//#include <wayland-text-server-protocol.h>

#include "wayland/global.h"

namespace Wrapland::Server
{
constexpr uint32_t TextInputManagerV3Version = 1;
using TextInputManagerV3Global = Wayland::Global<TextInputManagerV3, TextInputManagerV3Version>;
using TextInputManagerV3Bind = Wayland::Bind<TextInputManagerV3Global>;

class TextInputManagerV3::Private : public TextInputManagerV3Global
{
public:
    Private(Display* display, TextInputManagerV3 *q);

protected:
    //void zwp_text_input_manager_v3_destroy(Resource *resource) override;
    //void zwp_text_input_manager_v3_get_text_input(Resource *resource, uint32_t id, wl_resource *seat) override;

private:
    static void getTextInputCallback(TextInputManagerV3Bind *bind, uint32_t id, wl_resource *wlSeat);

    static const struct zwp_text_input_manager_v3_interface s_interface;
};

class TextInputV3::Private : public Wayland::Resource<TextInputV3>
{
public:
    Private(Client* client, uint32_t version, uint32_t id, TextInputV3* q);

    // events
    void sendEnter(Surface* surface);
    void sendLeave(Surface* surface);
    void preEdit(const QByteArray& text, quint32 cursorBegin, quint32 cursorEnd);
    void commitString(const QByteArray& text);
    void deleteSurroundingText(uint32_t beforeLength, uint32_t afterLength);
    void done(uint32_t serial);

//    QList<TextInputV3InterfacePrivate::Resource *> textInputsForClient(ClientConnection *client) const;

//    static TextInputV3InterfacePrivate *get(TextInputV3Interface *inputInterface) { return inputInterface->d.data(); }

    Seat *seat = nullptr;

    struct TextInputV3State current;
    struct TextInputV3State pending;

    QHash<Resource *, quint32> serialHash;

    TextInputV3 *q;

private:
    static const struct zwp_text_input_v3_interface s_interface;

    static void enableCallback(wl_client* wlClient, wl_resource* wlResource);
    static void disableCallback(wl_client* wlClient, wl_resource* wlResource);

    static void setSurroundingTextCallback(wl_client* wlClient,
                                           wl_resource* wlResource,
                                           const char* text,
                                           int32_t cursor,
                                           int32_t anchor);
    static void setContentTypeCallback(wl_client* wlClient,
                                       wl_resource* wlResource,
                                       uint32_t hint,
                                       uint32_t purpose);
    static void setCursorRectangleCallback(wl_client* wlClient,
                                           wl_resource* wlResource,
                                           int32_t x,
                                           int32_t y,
                                           int32_t width,
                                           int32_t height);
    static void setTextChangeCauseCallback(wl_client* wlClient,
                                       wl_resource* wlResource,
                                       uint32_t change_cause);
    static void setCommitCallback(wl_client* wlClient,
                                                      wl_resource* wlResource);
    void enable();
    void disable();
/*
protected:
    void zwp_text_input_v3_bind_resource(Resource *resource) override;
    void zwp_text_input_v3_destroy(Resource *resource) override;
    // requests
    void zwp_text_input_v3_enable(Resource *resource) override;
    void zwp_text_input_v3_disable(Resource *resource) override;
    void zwp_text_input_v3_set_surrounding_text(Resource *resource, const QString &text, int32_t cursor, int32_t anchor) override;
    void zwp_text_input_v3_set_content_type(Resource *resource, uint32_t hint, uint32_t purpose) override;
    void zwp_text_input_v3_set_text_change_cause(Resource *resource, uint32_t cause) override;
    void zwp_text_input_v3_set_cursor_rectangle(Resource *resource, int32_t x, int32_t y, int32_t width, int32_t height) override;
    void zwp_text_input_v3_commit(Resource * resource) override;
*/
};
}
