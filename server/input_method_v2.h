/*
    SPDX-FileCopyrightText: 2020 Aleix Pol Gonzalez <aleixpol@kde.org>
    2021 Dorota Czaplejewicz <gihuac.dcz@porcupinefactory.org>

    SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL
*/

#pragma once

#include <memory>
#include <QObject>

#include <Wrapland/Server/wraplandserver_export.h>
#include "wayland/client.h"
#include "text_input_v3.h"

namespace Wrapland::Server
{
class Display;
class Seat;
class Surface;

class InputMethodV2;

class InputMethodManagerV2Private;
class InputMethodV2Private;

/* This file's classes implement input_method_unstable_v2,
 * with the omission of popup.
 */

/**
 * Implements input_method_manager_v2
 */
class WRAPLANDSERVER_EXPORT InputMethodManagerV2 : public QObject
{
    Q_OBJECT
public:
    explicit InputMethodManagerV2(Display *d, QObject *parent = nullptr);
    ~InputMethodManagerV2() override;

private:
    class Private;
    std::unique_ptr<Private> d_ptr;
};

/**
 * Implements input_method_v2, allows to describe the client's input state
 */
class WRAPLANDSERVER_EXPORT InputMethodV2 : public QObject
{
    Q_OBJECT
public:
    ~InputMethodV2() override;

    void sendSurroundingText(const QString &text, quint32 cursor, quint32 anchor);
    void sendContentType(Wrapland::Server::TextInputV3ContentHints hint, Wrapland::Server::TextInputV3ContentPurpose purpose);
    void sendCommitState(quint32 serial);

Q_SIGNALS:
    void commitString(const QString &text);
    void setPreeditString(const QString &text, int32_t cursor_begin, int32_t cursor_end);
    void deleteSurroundingText(qint32 index, quint32 length);
    void commit();
    // Not implemented
    //void grabKeyboard(quint32 keyboard);
    void resourceDestroyed();

private:
    explicit InputMethodV2(Client* client, uint32_t version, uint32_t id);
    friend class InputMethodManagerV2;
    friend class InputMethodV2Private;

    class Private;
    Private* d_ptr;
};
}

Q_DECLARE_METATYPE(Wrapland::Server::InputMethodV2 *)
