/*
    SPDX-FileCopyrightText: 2020 Bhushan Shah <bshah@kde.org>
    SPDX-FileCopyrightText: 2018 Roman Glig <subdiff@gmail.com>

    SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL
*/

#include "text_input_v3.h"
#include "display.h"
#include "wayland/global.h"
#include "seat_p.h"
#include "surface_p.h"
#include "text_input_v3_p.h"

namespace Wrapland::Server
{

const struct zwp_text_input_manager_v3_interface TextInputManagerV3::Private::s_interface = {
    resourceDestroyCallback,
    cb<getTextInputCallback>,
};

static TextInputV3::ContentHints convertContentHint(uint32_t hint)
{
    const auto hints = zwp_text_input_v3_content_hint(hint);
    TextInputV3::ContentHints ret = TextInputV3::ContentHint::None;

    if (hints & ZWP_TEXT_INPUT_V3_CONTENT_HINT_AUTO_CAPITALIZATION) {
        ret |= TextInputV3::ContentHint::AutoCapitalization;
    }
    if (hints & ZWP_TEXT_INPUT_V3_CONTENT_HINT_LOWERCASE) {
        ret |= TextInputV3::ContentHint::LowerCase;
    }
    if (hints & ZWP_TEXT_INPUT_V3_CONTENT_HINT_UPPERCASE) {
        ret |= TextInputV3::ContentHint::UpperCase;
    }
    if (hints & ZWP_TEXT_INPUT_V3_CONTENT_HINT_TITLECASE) {
        ret |= TextInputV3::ContentHint::TitleCase;
    }
    if (hints & ZWP_TEXT_INPUT_V3_CONTENT_HINT_HIDDEN_TEXT) {
        ret |= TextInputV3::ContentHint::HiddenText;
    }
    if (hints & ZWP_TEXT_INPUT_V3_CONTENT_HINT_SENSITIVE_DATA) {
        ret |= TextInputV3::ContentHint::SensitiveData;
    }
    if (hints & ZWP_TEXT_INPUT_V3_CONTENT_HINT_LATIN) {
        ret |= TextInputV3::ContentHint::Latin;
    }
    if (hints & ZWP_TEXT_INPUT_V3_CONTENT_HINT_MULTILINE) {
        ret |= TextInputV3::ContentHint::MultiLine;
    }
    return ret;
}

uint32_t convertContentHints(TextInputV3ContentHints hints)
{
    uint32_t ret = 0;

    if (hints & TextInputV3::ContentHint::AutoCapitalization) {
        ret |= ZWP_TEXT_INPUT_V3_CONTENT_HINT_AUTO_CAPITALIZATION;
    }
    if (hints & TextInputV3::ContentHint::LowerCase) {
        ret |= ZWP_TEXT_INPUT_V3_CONTENT_HINT_LOWERCASE;
    }
    if (hints & TextInputV3::ContentHint::UpperCase) {
        ret |= ZWP_TEXT_INPUT_V3_CONTENT_HINT_UPPERCASE;
    }
    if (hints & TextInputV3::ContentHint::TitleCase) {
        ret |= ZWP_TEXT_INPUT_V3_CONTENT_HINT_TITLECASE;
    }
    if (hints & TextInputV3::ContentHint::HiddenText) {
        ret |= ZWP_TEXT_INPUT_V3_CONTENT_HINT_HIDDEN_TEXT;
    }
    if (hints & TextInputV3::ContentHint::SensitiveData) {
        ret |= ZWP_TEXT_INPUT_V3_CONTENT_HINT_SENSITIVE_DATA;
    }
    if (hints & TextInputV3::ContentHint::Latin) {
        ret |= ZWP_TEXT_INPUT_V3_CONTENT_HINT_LATIN;
    }
    if (hints & TextInputV3::ContentHint::MultiLine) {
        ret |= ZWP_TEXT_INPUT_V3_CONTENT_HINT_MULTILINE;
    }
    return ret;
}


static TextInputV3::ContentPurpose convertContentPurpose(uint32_t purpose)
{
    const auto wlPurpose = zwp_text_input_v3_content_purpose(purpose);

    switch (wlPurpose) {
    case ZWP_TEXT_INPUT_V3_CONTENT_PURPOSE_ALPHA:
        return TextInputV3::ContentPurpose::Alpha;
    case ZWP_TEXT_INPUT_V3_CONTENT_PURPOSE_DIGITS:
        return TextInputV3::ContentPurpose::Digits;
    case ZWP_TEXT_INPUT_V3_CONTENT_PURPOSE_NUMBER:
        return TextInputV3::ContentPurpose::Number;
    case ZWP_TEXT_INPUT_V3_CONTENT_PURPOSE_PHONE:
        return TextInputV3::ContentPurpose::Phone;
    case ZWP_TEXT_INPUT_V3_CONTENT_PURPOSE_URL:
        return TextInputV3::ContentPurpose::Url;
    case ZWP_TEXT_INPUT_V3_CONTENT_PURPOSE_EMAIL:
        return TextInputV3::ContentPurpose::Email;
    case ZWP_TEXT_INPUT_V3_CONTENT_PURPOSE_NAME:
        return TextInputV3::ContentPurpose::Name;
    case ZWP_TEXT_INPUT_V3_CONTENT_PURPOSE_PASSWORD:
        return TextInputV3::ContentPurpose::Password;
    case ZWP_TEXT_INPUT_V3_CONTENT_PURPOSE_DATE:
        return TextInputV3::ContentPurpose::Date;
    case ZWP_TEXT_INPUT_V3_CONTENT_PURPOSE_TIME:
        return TextInputV3::ContentPurpose::Time;
    case ZWP_TEXT_INPUT_V3_CONTENT_PURPOSE_DATETIME:
        return TextInputV3::ContentPurpose::DateTime;
    case ZWP_TEXT_INPUT_V3_CONTENT_PURPOSE_TERMINAL:
        return TextInputV3::ContentPurpose::Terminal;
    case ZWP_TEXT_INPUT_V3_CONTENT_PURPOSE_NORMAL:
    default:
        return TextInputV3::ContentPurpose::Normal;
    }
}

uint32_t convertContentPurpose(TextInputV3ContentPurpose purpose)
{
    switch (purpose) {
    case TextInputV3::ContentPurpose::Alpha:
        return ZWP_TEXT_INPUT_V3_CONTENT_PURPOSE_ALPHA;
    case TextInputV3::ContentPurpose::Digits:
        return ZWP_TEXT_INPUT_V3_CONTENT_PURPOSE_DIGITS;
    case TextInputV3::ContentPurpose::Number:
        return ZWP_TEXT_INPUT_V3_CONTENT_PURPOSE_NUMBER;
    case TextInputV3::ContentPurpose::Phone:
        return ZWP_TEXT_INPUT_V3_CONTENT_PURPOSE_PHONE;
    case TextInputV3::ContentPurpose::Url:
        return ZWP_TEXT_INPUT_V3_CONTENT_PURPOSE_URL;
    case TextInputV3::ContentPurpose::Email:
        return ZWP_TEXT_INPUT_V3_CONTENT_PURPOSE_EMAIL;
    case TextInputV3::ContentPurpose::Name:
        return ZWP_TEXT_INPUT_V3_CONTENT_PURPOSE_NAME;
    case TextInputV3::ContentPurpose::Password:
        return ZWP_TEXT_INPUT_V3_CONTENT_PURPOSE_PASSWORD;
    case TextInputV3::ContentPurpose::Date:
        return ZWP_TEXT_INPUT_V3_CONTENT_PURPOSE_DATE;
    case TextInputV3::ContentPurpose::Time:
        return ZWP_TEXT_INPUT_V3_CONTENT_PURPOSE_TIME;
    case TextInputV3::ContentPurpose::DateTime:
        return ZWP_TEXT_INPUT_V3_CONTENT_PURPOSE_DATETIME;
    case TextInputV3::ContentPurpose::Terminal:
        return ZWP_TEXT_INPUT_V3_CONTENT_PURPOSE_TERMINAL;
    case TextInputV3::ContentPurpose::Normal:
    default:
        return ZWP_TEXT_INPUT_V3_CONTENT_PURPOSE_NORMAL;
    }
}

static TextInputV3::ChangeCause convertChangeCause(uint32_t cause)
{
    const auto wlCause = zwp_text_input_v3_change_cause(cause);

    switch (wlCause) {
    case ZWP_TEXT_INPUT_V3_CHANGE_CAUSE_INPUT_METHOD:
        return TextInputV3::ChangeCause::InputMethod;
    case ZWP_TEXT_INPUT_V3_CHANGE_CAUSE_OTHER:
    default:
        return TextInputV3::ChangeCause::Other;
    }
}

uint32_t convertChangeCause(TextInputV3ChangeCause cause)
{
    switch (cause) {
    case TextInputV3::ChangeCause::InputMethod:
        return ZWP_TEXT_INPUT_V3_CHANGE_CAUSE_INPUT_METHOD;
    case TextInputV3::ChangeCause::Other:
    default:
        return ZWP_TEXT_INPUT_V3_CHANGE_CAUSE_OTHER;
    }
}

TextInputManagerV3::Private::Private(Display* display, TextInputManagerV3* q)
    : TextInputManagerV3Global(q, display, &zwp_text_input_manager_v3_interface, &s_interface)
{
    create();
}

void TextInputManagerV3::Private::getTextInputCallback(TextInputManagerV3Bind* bind,
                                                       uint32_t id,
                                                       wl_resource* wlSeat)
{
    auto seat = SeatGlobal::handle(wlSeat);

    auto textInput = new TextInputV3(bind->client()->handle(), bind->version(), id);

    textInput->d_ptr->seat = seat;

    seat->d_ptr->registerTextInput(textInput);
}

TextInputManagerV3::TextInputManagerV3(Display* display, QObject* parent)
    : QObject(parent)
    , d_ptr(new Private(display, this))
{
}

TextInputManagerV3::~TextInputManagerV3() = default;

const struct zwp_text_input_v3_interface TextInputV3::Private::s_interface = {
    destroyCallback,
    enableCallback,
    disableCallback,
    setSurroundingTextCallback,
    setTextChangeCauseCallback,
    setContentTypeCallback,
    setCursorRectangleCallback,
    setCommitCallback,
};

TextInputV3::Private::Private(Client* client, uint32_t version, uint32_t id, TextInputV3* q)
    : Wayland::Resource<TextInputV3>(client,
                                     version,
                                     id,
                                     &zwp_text_input_v3_interface,
                                     &s_interface,
                                     q)
{
}

void TextInputV3::Private::enable()
{
    pending.enabled = true;
}

void TextInputV3::Private::disable()
{
    pending.enabled = false;
}

void TextInputV3::Private::sendEnter(Surface* surface)
{
    if (!surface) {
        return;
    }
    send<zwp_text_input_v3_send_enter>(surface->d_ptr->resource());
}

void TextInputV3::Private::sendLeave(Surface* surface)
{
    if (!surface) {
        return;
    }
    send<zwp_text_input_v3_send_leave>(surface->d_ptr->resource());
}

void TextInputV3::Private::preEdit(const QByteArray& text, uint32_t cursorBegin, uint32_t cursorEnd)
{
    send<zwp_text_input_v3_send_preedit_string>(text.constData(), cursorBegin, cursorEnd);
}

void TextInputV3::Private::commitString(const QByteArray& text)
{
    send<zwp_text_input_v3_send_commit_string>(text.constData());
}

void TextInputV3::Private::deleteSurroundingText(quint32 beforeLength, quint32 afterLength)
{
    send<zwp_text_input_v3_send_delete_surrounding_text>(beforeLength, afterLength);
}

void TextInputV3::Private::done(uint32_t serial)
{
    send<zwp_text_input_v3_send_done>(serial);
}

void TextInputV3::Private::enableCallback([[maybe_unused]] wl_client* wlClient,
                                          wl_resource* wlResource)
{
    auto priv = handle(wlResource)->d_ptr;
    priv->enable();
}

void TextInputV3::Private::disableCallback([[maybe_unused]] wl_client* wlClient,
                                           wl_resource* wlResource)
{
    auto priv = handle(wlResource)->d_ptr;
    priv->disable();
}


void TextInputV3::Private::setSurroundingTextCallback([[maybe_unused]] wl_client* wlClient,
                                                      wl_resource* wlResource,
                                                      const char* text,
                                                      int32_t cursor,
                                                      int32_t anchor)
{
    auto priv = handle(wlResource)->d_ptr;

    priv->pending.surroundingText = QByteArray(text);
    priv->pending.surroundingTextCursorPosition = cursor;
    priv->pending.surroundingTextSelectionAnchor = anchor;
}

void TextInputV3::Private::setContentTypeCallback([[maybe_unused]] wl_client* wlClient,
                                                  wl_resource* wlResource,
                                                  uint32_t hint,
                                                  uint32_t purpose)
{
    auto priv = handle(wlResource)->d_ptr;
    const auto contentHints = convertContentHint(hint);
    const auto contentPurpose = convertContentPurpose(purpose);

    priv->pending.contentHints = contentHints;
    priv->pending.contentPurpose = contentPurpose;
}

void TextInputV3::Private::setCursorRectangleCallback([[maybe_unused]] wl_client* wlClient,
                                                      wl_resource* wlResource,
                                                      int32_t x,
                                                      int32_t y,
                                                      int32_t width,
                                                      int32_t height)
{
    auto priv = handle(wlResource)->d_ptr;
    const QRect rect = QRect(x, y, width, height);

    priv->pending.cursorRectangle = rect;
}


void TextInputV3::Private::setTextChangeCauseCallback([[maybe_unused]] wl_client* wlClient,
                                                  wl_resource* wlResource,
                                                  uint32_t cause)
{
    auto priv = handle(wlResource)->d_ptr;
    const auto changeCause = convertChangeCause(cause);

    priv->pending.surroundingTextChangeCause = changeCause;
}

void TextInputV3::Private::setCommitCallback([[maybe_unused]] wl_client* wlClient,
                                                  wl_resource* wlResource)
{
    auto priv = handle(wlResource)->d_ptr;
    auto old = priv->current;

    // Let all signals see new value.
    priv->current = priv->pending;
    //serialHash[resource]++;


    if (old.enabled != priv->pending.enabled) {
        Q_EMIT priv->handle()->enabledChanged();
    }
    // FIXME: enable handling not inspected properly yet.
    // The compositor should decide whether to ignore events or not,
    // so generally enable shouldn't be taken into account here yet,
    // unless forbidden by the protocol.

    // FIXME: make sure the new pending is initialized OK.

    // FIXME: following changes are detected naively,
    // by comparing the default and the set value.
    // This will not always be correct!
    // For example, the text input sends surrounding text
    // only if it wants to indicate support.
    // A comparison with default will miss
    // that text input used the default value to communicate support.
    // There should be a way to distinguish "modified" from "unmodified",
    // according to the text_input protocol spec.
    if (old.contentHints != priv->pending.contentHints || old.contentPurpose != priv->pending.contentPurpose) {
        if (priv->pending.enabled) {
            Q_EMIT priv->handle()->contentTypeChanged();
        }
    }

    if (old.cursorRectangle != priv->pending.cursorRectangle) {
        if (priv->pending.enabled) {
            Q_EMIT priv->handle()->cursorRectangleChanged(priv->pending.cursorRectangle);
        }
    }

    if (old.surroundingText != priv->pending.surroundingText
            || old.surroundingTextCursorPosition != priv->pending.surroundingTextCursorPosition
            || old.surroundingTextSelectionAnchor != priv->pending.surroundingTextSelectionAnchor) {
        if (priv->pending.enabled) {
            Q_EMIT priv->handle()->surroundingTextChanged();
        }
    }
    if (old.surroundingTextChangeCause == TextInputV3ChangeCause::Other) {
        priv->pending.surroundingTextChangeCause = TextInputV3::ChangeCause::InputMethod;
        Q_EMIT priv->handle()->surroundingTextChanged();
    }

    // FIXME: does this need serial?
    Q_EMIT priv->handle()->stateCommitted(0);//serialHash[resource]);
}

/*
void TextInputV3::Private::defaultPending()
{
    pending.cursorRectangle = QRect();
    pending.surroundingTextChangeCause = TextInputV3::ChangeCause::InputMethod;
    pending.contentHints = TextInputV3::ContentHints(TextInputV3::ContentHint::None);
    pending.contentPurpose = TextInputV3::ContentPurpose::Normal;
    pending.enabled = false;
    pending.surroundingText = QByteArray();
    pending.surroundingTextCursorPosition = 0;
    pending.surroundingTextSelectionAnchor = 0;
}*/

TextInputV3::TextInputV3(Client* client, uint32_t version, uint32_t id)
    : QObject(nullptr)
    , d_ptr(new Private(client, version, id, this))
{
}

TextInputV3::ContentHints TextInputV3::contentHints() const
{
    return d_ptr->current.contentHints;
}

TextInputV3::ContentPurpose TextInputV3::contentPurpose() const
{

    return d_ptr->current.contentPurpose;
}

QByteArray TextInputV3::surroundingText() const
{
    return d_ptr->current.surroundingText;
}

qint32 TextInputV3::surroundingTextCursorPosition() const
{
    return d_ptr->current.surroundingTextCursorPosition;
}

qint32 TextInputV3::surroundingTextSelectionAnchor() const
{
    return d_ptr->current.surroundingTextSelectionAnchor;
}

void TextInputV3::setPreEditString(const QByteArray &text, quint32 cursorBegin, quint32 cursorEnd)
{
    d_ptr->preEdit(text, cursorBegin, cursorEnd);
}

void TextInputV3::commitString(const QByteArray& text)
{

    d_ptr->commitString(text);
}
void TextInputV3::deleteSurroundingText(quint32 beforeLength, quint32 afterLength)
{
    d_ptr->deleteSurroundingText(beforeLength, afterLength);
}

void TextInputV3::done(uint32_t serial)
{
    d_ptr->done(serial);
}

QRect TextInputV3::cursorRectangle() const
{
    return d_ptr->current.cursorRectangle;
}

bool TextInputV3::isEnabled() const
{
    return d_ptr->current.enabled;
}

const TextInputV3State &TextInputV3::state() const {
    return d_ptr->current;
}

Client* TextInputV3::client() const
{
    return d_ptr->client()->handle();
}
}
