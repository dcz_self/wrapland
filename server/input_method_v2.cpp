/*
    SPDX-FileCopyrightText: 2020 Aleix Pol Gonzalez <aleixpol@kde.org>
    2021 Dorota Czaplejewicz <gihuac.dcz@porcupinefactory.org>

    SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL
*/

#include "input_method_v2.h"
#include "seat_p.h"
#include "display.h"
#include "surface_p.h"

#include <QHash>

#include <wayland-input-method-unstable-v2-server-protocol.h>

namespace Wrapland::Server
{
class InputMethodV2::Private : public Wayland::Resource<InputMethodV2>
{
private:
    const struct zwp_input_method_v2_interface s_interface = {
        commitStringCallback,
        preeditStringCallback,
        deleteSurroundingTextCallback,
        commitCallback,
        getInputPopupSurfaceCallback,
        grabKeyboardCallback,
        destroyCallback,
    };

public:
    Private(Client* client, uint32_t version, uint32_t id, InputMethodV2* q)
        : Wayland::Resource<InputMethodV2>(client,
                                         version,
                                         id,
                                         &zwp_input_method_v2_interface,
                                         &s_interface,
                                         q)
    {
    }

    void sendActivate() {
        send<zwp_input_method_v2_send_activate>();
    }
    void sendDeactivate() {
        send<zwp_input_method_v2_send_activate>();
    }
    void sendSurroundingText(QByteArray &text, uint32_t cursor, uint32_t anchor) {
        send<zwp_input_method_v2_send_surrounding_text>(text.constData(), cursor, anchor);
    }
    void sendTextChangeCause(TextInputV3ChangeCause cause) {
        send<zwp_input_method_v2_send_text_change_cause>(convertChangeCause(cause));
    }
    void sendContentType(TextInputV3ContentHints hints, TextInputV3ContentPurpose purpose) {
        send<zwp_input_method_v2_send_content_type>(convertContentHints(hints), convertContentPurpose(purpose));
    }
    void sendDone() {
        send<zwp_input_method_v2_send_done>();
    }
    void sendUnavailable() {
        send<zwp_input_method_v2_send_unavailable>();
    }

    Seat*seat = nullptr;

    struct State {
        //bool enabled = false;
        QByteArray commitText;
        QByteArray preeditString;
        int32_t preeditTextCursorBegin = 0;
        int32_t preeditTextCursorEnd = 0;
        uint32_t deleteBytesBefore = 0;
        uint32_t deleteBytesAfter = 0;
    } current, pending;

    InputMethodV2 *q;

private:

    static void commitStringCallback([[maybe_unused]]
    wl_client* wlClient,
    wl_resource* wlResource,
    const char *text)
    {
        auto priv = handle(wlResource)->d_ptr;
        priv->pending.commitText = text;
    }

    static void preeditStringCallback([[maybe_unused]]
    wl_client* wlClient,
    wl_resource* wlResource,
    const char* text,
    int32_t cursorBegin,
    int32_t cursorEnd)
    {
        auto priv = handle(wlResource)->d_ptr;

        priv->pending.preeditString = text;
        priv->pending.preeditTextCursorBegin = cursorBegin;
        priv->pending.preeditTextCursorEnd = cursorEnd;

    }

    static void deleteSurroundingTextCallback([[maybe_unused]]
    wl_client* wlClient,
    wl_resource* wlResource,
    uint32_t beforeBytes,
    uint32_t afterBytes)
    {
        auto priv = handle(wlResource)->d_ptr;
        priv->pending.deleteBytesBefore = beforeBytes;
        priv->pending.deleteBytesAfter = afterBytes;
    }

    static void commitCallback([[maybe_unused]]
    wl_client* wlClient,
    wl_resource* wlResource,
    uint32_t serial)
    {
        auto priv = handle(wlResource)->d_ptr;
        // FIXME: emit signals for each piece
        Q_EMIT priv->handle()->commit();
    }
    static void getInputPopupSurfaceCallback([[maybe_unused]]
    wl_client* wlClient,
    wl_resource* wlResource,
    uint32_t id,
    wl_resource* surface)
    {
        auto priv = handle(wlResource)->d_ptr;
        // FIXME: not implemented
    }

    static void grabKeyboardCallback([[maybe_unused]]
    wl_client* wlClient,
    wl_resource* wlResource,
    uint32_t keyboard)
    {
        auto priv = handle(wlResource)->d_ptr;
        // FIXME: not implemented
    }
};

InputMethodV2::InputMethodV2(Client* client, uint32_t version, uint32_t id)
    : QObject(nullptr)
    , d_ptr(new Private(client, version, id, this))
{
}


InputMethodV2::~InputMethodV2() = default;


constexpr uint32_t InputMethodManagerV2Version = 1;
using InputMethodManagerV2Global = Wayland::Global<InputMethodManagerV2, InputMethodManagerV2Version>;
using InputMethodManagerV2Bind = Wayland::Bind<InputMethodManagerV2Global>;


class InputMethodManagerV2::Private : public InputMethodManagerV2Global
{
private:
    static void getInputMethodCallback(InputMethodManagerV2Bind* bind,
                                       wl_resource* wlSeat,
                                       uint32_t id)
    {
        auto seat = SeatGlobal::handle(wlSeat);
        auto inputMethod = new InputMethodV2(bind->client()->handle(), bind->version(), id);

        inputMethod->d_ptr->seat = seat;
        //seat->d_ptr->registerTextInput(textInput);
    }

    const struct zwp_input_method_manager_v2_interface s_interface = {
        cb<getInputMethodCallback>,
        resourceDestroyCallback,
    };

public:
    Private(Display* display, InputMethodManagerV2* q)
            : InputMethodManagerV2Global(q, display, &zwp_input_method_manager_v2_interface, &s_interface)
    {
        create();
    }
};

InputMethodManagerV2::InputMethodManagerV2(Display *display, QObject *parent)
    : QObject(parent)
    , d_ptr(new Private(display, this))
{
}

InputMethodManagerV2::~InputMethodManagerV2() = default;
}
