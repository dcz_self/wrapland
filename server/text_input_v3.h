/*
    SPDX-FileCopyrightText: 2018 Roman Glig <subdiff@gmail.com>
    SPDX-FileCopyrightText: 2020 Bhushan Shah <bshah@kde.org>

    SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL
*/
#pragma once

#include <memory>
#include <QObject>
#include <QRect>

#include <Wrapland/Server/wraplandserver_export.h>

//#include "wayland/client.h"

struct wl_resource;
namespace Wrapland::Server
{
class Client;
class Display;
class Seat;
class Surface;


enum class TextInputV3ContentHint : uint32_t {
    None = 0,
    AutoCompletion = 1 << 0,
    AutoCorrection = 1 << 1,
    AutoCapitalization = 1 << 2,
    LowerCase = 1 << 3,
    UpperCase = 1 << 4,
    TitleCase = 1 << 5,
    HiddenText = 1 << 6,
    SensitiveData = 1 << 7,
    Latin = 1 << 8,
    MultiLine = 1 << 9,
};
Q_DECLARE_FLAGS(TextInputV3ContentHints, TextInputV3ContentHint)

uint32_t convertContentHints(TextInputV3ContentHints hints);

enum class TextInputV3ContentPurpose : uint32_t {
    Normal,
    Alpha,
    Digits,
    Number,
    Phone,
    Url,
    Email,
    Name,
    Password,
    Date,
    Time,
    DateTime,
    Terminal,
};
uint32_t convertContentPurpose(TextInputV3ContentPurpose purpose);

enum class TextInputV3ChangeCause {
    /**
     * Change caused by input method
     */
    InputMethod,

    /**
     * Something else other than input method caused change
     */
    Other
};

uint32_t convertChangeCause(TextInputV3ChangeCause cause);

struct TextInputV3State {
    QRect cursorRectangle;
    TextInputV3ContentHints contentHints = TextInputV3ContentHint::None;
    TextInputV3ContentPurpose contentPurpose = TextInputV3ContentPurpose::Normal;
    bool enabled = false;
    QByteArray surroundingText;
    qint32 surroundingTextCursorPosition = 0;
    qint32 surroundingTextSelectionAnchor = 0;
    TextInputV3ChangeCause surroundingTextChangeCause = TextInputV3ChangeCause::InputMethod;
};

/**
 * @brief Represent the Global for the interface.
 *
 * The class can represent different interfaces. Which concrete interface is represented
 * can be determined through {@link interfaceVersion}.
 *
 * To create a TextInputManagerV3 use {@link Display::createTextInputManager}
 *
 * @since 5.21
 **/
class WRAPLANDSERVER_EXPORT TextInputManagerV3 : public QObject
{
    Q_OBJECT
public:
    explicit TextInputManagerV3(Display *display, QObject *parent = nullptr);
    ~TextInputManagerV3() override;

private:
    class Private;
    std::unique_ptr<Private> d_ptr;
};

/**
 * @brief Represents a generic Resource for a text input object.
 * *
 * A TextInputV3Interface gets created by the {@link TextInputManagerV3}. The individual
 * instances are not exposed directly. The SeatInterface provides access to the currently active
 * TextInputV3Interface. This is evaluated automatically based on which SurfaceInterface has
 * keyboard focus.
 *
 * @see TextInputManagerV3
 * @see SeatInterface
 * @since 5.21
 **/
class WRAPLANDSERVER_EXPORT TextInputV3 : public QObject
{
    Q_OBJECT
public:
    Client* client() const;

    typedef TextInputV3ContentHint ContentHint;
    typedef TextInputV3ContentHints ContentHints;
    typedef TextInputV3ContentPurpose ContentPurpose;
    typedef TextInputV3ChangeCause ChangeCause;

    /**
     * @see cursorRectangleChanged
     **/
    QRect cursorRectangle() const;

    /**
     * @see contentTypeChanged
     **/
    ContentPurpose contentPurpose() const;

    /**
     *@see contentTypeChanged
     **/
    ContentHints contentHints() const;

    /**
     * @returns The plain surrounding text around the input position.
     * @see surroundingTextChanged
     * @see surroundingTextCursorPosition
     * @see surroundingTextSelectionAnchor
     **/
    QByteArray surroundingText() const;
    /**
     * @returns The byte offset of current cursor position within the {@link surroundingText}
     * @see surroundingText
     * @see surroundingTextChanged
     **/
    qint32 surroundingTextCursorPosition() const;
    /**
     * The byte offset of the selection anchor within the {@link surroundingText}.
     *
     * If there is no selected text this is the same as cursor.
     * @return The byte offset of the selection anchor
     * @see surroundingText
     * @see surroundingTextChanged
     **/
    qint32 surroundingTextSelectionAnchor() const;

    // Is this the right way to return an immutable reference?
    const TextInputV3State &state() const;

    /**
     * @return The surface the TextInputV3 is enabled on
     * @see isEnabled
     * @see enabledChanged
     **/
    QPointer<Surface> surface() const;

    /**
     * @return Whether the TextInputV3 is currently enabled for a Surface.
     * @see surface
     * @see enabledChanged
     **/
    bool isEnabled() const;

    /**
     * Notify when the text around the current cursor position should be deleted.
     *
     * The Client processes this event together with the commit string
     *
     * @param beforeLength length of text before current cursor position.
     * @param afterLength length of text after current cursor position.
     * @see commit
     **/
    void deleteSurroundingText(quint32 beforeLength, quint32 afterLength);
    
    /**
     * Send preEditString to the client
     *
     * @param text pre-edit string
     * @param cursorBegin
     * @param cursorEnd
     **/
    void setPreEditString(const QByteArray &text, quint32 cursorBegin, quint32 cursorEnd);
    
    /**
     * Notify when @p text should be inserted into the editor widget.
     * The text to commit could be either just a single character after a key press or the
     * result of some composing ({@link preEdit}). It could be also an empty text
     * when some text should be removed (see {@link deleteSurroundingText}) or when
     * the input cursor should be moved (see {@link cursorPosition}).
     *
     * Any previously set composing text should be removed.
     * @param text The utf8-encoded text to be inserted into the editor widget
     * @see preEdit
     * @see deleteSurroundingText
     **/
    void commitString(const QByteArray &text);
    
    /**
     * Notify when changes and state requested by sendPreEditString, commitString and deleteSurroundingText
     * should be applied.
     **/
    void done(uint32_t serial);


Q_SIGNALS:

    /**
     * @see cursorRectangle
     **/
    void cursorRectangleChanged(const QRect &rect);
    /**
     * Emitted when the {@link contentPurpose} and/or {@link contentHints} changes.
     * @see contentPurpose
     * @see contentHints
     **/
    void contentTypeChanged();

    /**
     * Emitted when the {@link surroundingText}, {@link surroundingTextCursorPosition}
     * and/or {@link surroundingTextSelectionAnchor} changed.
     * @see surroundingText
     * @see surroundingTextCursorPosition
     * @see surroundingTextSelectionAnchor
     **/
    void surroundingTextChanged();

    /**
     * Emitted whenever this TextInputV3Interface gets enabled or disabled for a SurfaceInterface.
     * @see isEnabled
     * @see surface
     **/
    void enabledChanged();

    /**
     * Emitted when state should be committed
     **/
    void stateCommitted(quint32 serial);

    void resourceDestroyed();

private:
    explicit TextInputV3(Client* client, uint32_t version, uint32_t id);
    friend class TextInputManagerV3;
    friend class Seat;

    class Private;
    Private* d_ptr;
};

}

Q_DECLARE_METATYPE(Wrapland::Server::TextInputV3 *)
Q_DECLARE_METATYPE(Wrapland::Server::TextInputV3ContentHint)
Q_DECLARE_METATYPE(Wrapland::Server::TextInputV3ContentHints)
Q_DECLARE_OPERATORS_FOR_FLAGS(Wrapland::Server::TextInputV3ContentHints)
Q_DECLARE_METATYPE(Wrapland::Server::TextInputV3ContentPurpose)
Q_DECLARE_METATYPE(Wrapland::Server::TextInputV3ChangeCause)
